FROM python:3.9-slim

RUN pip install -U pip setuptools wheel \
 && pip install httpie==2.6.0 \
 && http --debug --follow --check-status --output /tmp/install-poetry.py GET https://install.python-poetry.org/install-poetry.py \
 && pip freeze | sed 's/^\([A-Za-z][-A-Za-z0-9.]*\)==.*$/\1/' | xargs pip uninstall -y \
 && useradd --create-home --home-dir /build --no-user-group --shell /bin/bash build \
 && su build -c 'python /tmp/install-poetry.py' \
 && rm -f /tmp/install-poetry.py /build/.bash*

USER build
WORKDIR /build
ADD --chown=build:users bashrc /build/.bashrc
